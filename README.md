# Projetos de Lei que são relevantes para a privacidade, liberdade de expressão, acesso e questões de gênero no meio digital

(for English version, see [readme-en][readme-en])

Código-fonte do website <https://radarlegislativo.org/>.

Mantido pela [Coding Rights](https://codingrights.org), uma organização liderada por mulheres dedicada a promover a compreensão sobre o funcionamento de tecnologias digitais e expor as assimetrias de poder que podem ser ampliadas por seu uso.

Nosso trabalho envolve o monitoramento e análise de códigos legais, culturais e de programação para influenciar políticas públicas e incentivar boas práticas. Somos parte de uma rede global de ativistas que criam e compartilham ferramentas e estratégias para o uso mais autônomo e consciente das tecnologias e para a inclusão da perspectiva dos direitos humanos quando se pensa nos meios digitais.

Se você quer entender o código-fonte do site, veja [CONTRIBUTING.md][contributing].

[readme-en]: https://gitlab.com/codingrights/pls/blob/master/README.en.md
[contributing]: https://gitlab.com/codingrights/pls/blob/master/CONTRIBUTING.md
