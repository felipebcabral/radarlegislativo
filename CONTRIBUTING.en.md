Developer notes
===============


This document describes the basic configuration of the developer
environment. It describes how to run the project with example data. To
configure the process of capturing draft bills, please read
[`docs/captura.en.md`][captura].

Pre-requisites
--------------

### Local install (without Docker)

You must have Python installed in the machine, and the package installer
pip. In GNU/Linux systems, both may be found in repositories
(``python`` e ``python-pip``).

We recommend you to use virtualenv to facilitate the management of python
packages. With [virtualenv and virtualenvwrapper installed][guia-virtualenv],
you need to create the virtual environment:

```
$ mkvirtualenv radarlegislativo
```

Then, install the requirements in the terminal, being inside the repository
directory (the same of this file) and with virtualenv activated:

```
$ pip install --upgrade -r requirements.txt
```

It is also necessary that [elasticsearch 2.4][elasticsearch] is installed.

For Debian or Ubuntu GNU/Linux systems, you may just run the script included
in this repository:

```
$ ./scripts/get_elasticsearch.sh
```

This script will download elasticsearch and will let it available in the
place assumed by other scripts in this repository.

In case you are not using one of these Linux distributions, follow the
[installing instructions][elastic-install].

It is also necessary to install the [LESS][less-css] compiler. In Debian or
GNU/Linux systems, you may just use the package manager:

```
$ sudo apt install node-less
```

### Container install (with Docker)

Requires [Docker][docker] and [Docker Compose][docker-compose]:

```
$ docker-compose -f docker-compose.dev.yml build
```

Development
-----------

Create, in the project's root directory, a configuration file specific for
the environment (you may use the example included as a basis):

```
$ cp example-env .env
```

Edit the file so that the line that contains `DEBUG=False` turns to
`DEBUG=True`.

In the `.env` file, besides defining the variable `DEBUG` you may define
various other settings. For example, the contents of `SECRET_KEY` should be
unique for each installation and kept in secret for [security reasons][django_secret_key].

For tramitabot to work, you must define in this same file (`.env`) the
variable `TRAMITABOT_API_TOKEN`, with the value given in the [bot creation
process][telegram_bot]. If you don't need tramitabot running right now, you
may define any value for this variable in the meantime.

Other variables are described in the [deploy documentation][deploy].

### For local install (wihtout Docker)

Create the database and load it with the minimum required data:

```
$ make dev_init_db
```

and run the development environment

```
$ make dev
```

In a few seconds you may already access the platform on the address
<http://localhost:8000>.

On first execution (and only on the first one) it is necessary to update the
elasticsearch index. For that, you should execute (with `make dev` running
in another terminal):

```
$ python radarlegislativo/manage.py rebuild_index
```

To see changes in the website quickly, you may make changes while you keep
opened a terminal with ``make dev``.

If you plan to capture real data from Câmara and Senado websites, go further
to the [configuration of the process of capturing draft bills][captura]

### For container install (with Docker)

Run migrations:

```
$ docker-compose -f docker-compose.dev.yml run --rm web python manage.py migrate
```

Load data:

```
$ docker-compose -f docker-compose.dev.yml run --rm web python manage.py loaddata \
    main/fixtures/tags.json \
    main/fixtures/projetos.json \
    main/fixtures/tramitacoes.json \
    agenda/fixtures/comissoes.json
```

And build Elasticsearch indexes:

```
$ docker-compose -f docker-compose.dev.yml run --rm web python manage.py rebuild_index
```

To start the app:

```
$ docker-compose -f docker-compose.dev.yml up
```

Production
-----------

The website [Radar Legislativo][website] is still under tests. If you find
any error, please contact us.

[guia-virtualenv]: http://exponential.io/blog/2015/02/10/install-virtualenv-and-virtualenvwrapper-on-ubuntu/
[website]: https://radarlegislativo.org
[django_secret_key]: https://docs.djangoproject.com/en/1.11/ref/settings/#std:setting-SECRET_KEY
[telegram_bot]: https://core.telegram.org/bots#6-botfather
[deploy]: https://gitlab.com/codingrights/radarlegislativo/blob/master/docs/deploy.en.md
[captura]: https://gitlab.com/codingrights/radarlegislativo/blob/master/docs/captura.en.md
[elasticsearch]: https://www.elastic.co/guide/en/elasticsearch/reference/2.4/index.html
[elastic-install]: https://www.elastic.co/guide/en/elasticsearch/reference/2.4/_installation.html
[less-css]: http://lesscss.org/
[docker]: https://docs.docker.com/install/
[docker-compose]: https://docs.docker.com/compose/install/
