#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import unicode_literals
import datetime

from django.db import models
from django.utils.encoding import python_2_unicode_compatible
from django.utils.text import slugify
from django.urls import reverse

from markdownx.models import MarkdownxField

URL_API_SENADO_MATERIA = u'http://legis.senado.leg.br/dadosabertos/materia/completa/{}'
URL_API_SENADO_MOVIMENTACOES = u'http://legis.senado.leg.br/dadosabertos/materia/movimentacoes/{}'
URL_WEB_SENADO = u'http://www25.senado.leg.br/web/atividade/materias/-/materia/{}'

URL_API_CAMARA = u'http://www.camara.gov.br/SitCamaraWS/Proposicoes.asmx?wsdl'
URL_WEB_CAMARA = u'http://www.camara.gov.br/proposicoesWeb/fichadetramitacao?idProposicao={}'


@python_2_unicode_compatible
class Tag(models.Model):
    nome = models.CharField('tag', max_length=256)
    slug = models.SlugField(max_length=20, unique=True)
    icon = models.ImageField(null=True)

    def __str__(self):
        return self.nome


class ProjetoManager(models.Manager):
    def na_mesma_semana_que_o_dia(self, dia):
        semana = dia.isocalendar()[1]
        return self.filter(cadastro__week=semana)

    def novos(self):
        return self.na_mesma_semana_que_o_dia(datetime.date.today())


@python_2_unicode_compatible
class Projeto(models.Model):

    objects = ProjetoManager()

    CAMARA = "CA"
    SENADO = "SE"

    ORIGEM_CHOICES = (
        (CAMARA, "Câmara"),
        (SENADO, "Senado"),
    )

    VERDE = 1
    AMARELO = 2
    VERMELHO = 3

    IMPACTO_CHOICES = (
        (VERDE, '🙂️'),
        (AMARELO, "😐"),
        (VERMELHO, "🙁")
    )

    origem = models.CharField(max_length=2, choices=ORIGEM_CHOICES)
    id_site = models.IntegerField('id no site de origem')
    nome = models.CharField(max_length=255)
    tags = models.ManyToManyField(Tag)

    apresentacao = models.DateField()
    ementa = models.TextField()
    autoria = models.CharField(max_length=255)
    apensadas = models.TextField(blank=True)
    local = models.TextField(blank=True)

    html_original = models.TextField(blank=True)

    apelido = models.CharField(max_length=255, blank=True)
    importante = models.BooleanField(default=False)
    urgente = models.BooleanField(default=False)
    arquivado = models.BooleanField(default=False)
    impacto = models.IntegerField(choices=IMPACTO_CHOICES, default=AMARELO)
    informacoes_adicionais = MarkdownxField(blank=True)
    clippings = MarkdownxField(blank=True)
    pontos_principais = MarkdownxField(blank=True)
    texto_acao = models.TextField(blank=True)

    cadastro = models.DateField(auto_now_add=True)

    ultima_atualizacao = models.DateField(default=datetime.date.today)

    texto_acao.verbose_name = "texto de ação"

    class Meta:
        unique_together = (("origem", "id_site"),)
        ordering = ['-ultima_atualizacao']

    def tag_list(self):
        """
        Shows a list of tags for this project.

        We use this for the list_display in ProjetoAdmin, but it adds another
        (potentially big) query to that page. If it grows too slow, we can
        remove this (and not have a list of tags in the admin list_display).
        """
        return ', '.join(self.tags.values_list('nome',
            flat=True).order_by('nome'))

    @property
    def ultima_tramitacao(self):
        return self.tramitacao_set.latest()

    @property
    def ultimas_tramitacoes(self):
        return self.tramitacao_set.order_by('-data')

    @property
    def slug(self):
        return slugify(self.nome)

    @property
    def situacao(self):
        return "{} - {}".format(self.ultima_tramitacao.local, self.ultima_tramitacao.descricao)

    @property
    def link(self):
        if self.origem == self.SENADO:
            return URL_WEB_SENADO.format(self.id_site)
        elif self.origem == self.CAMARA:
            return URL_WEB_CAMARA.format(self.id_site)

    def get_absolute_url(self):
        return reverse("website:projeto", args=[self.pk])

    def __str__(self):
        return '{} - {}'.format(self.nome, self.get_origem_display())


@python_2_unicode_compatible
class Tramitacao(models.Model):
    id_site = models.CharField(max_length=255)
    data = models.DateField()
    projeto = models.ForeignKey(Projeto)
    local = models.CharField(max_length=255)
    descricao = models.TextField()

    class Meta:
        ordering = ["-data"]
        get_latest_by = "data"
        verbose_name_plural = "tramitações"

    def save(self, *args, **kwargs):
        result = super(Tramitacao, self).save(*args, **kwargs)
        self.projeto.ultima_atualizacao = self.data
        self.projeto.save()
        return result

    def get_absolute_url(self):
        return self.projeto.get_absolute_url()

    @property
    def descricao_limpa(self):
        string_to_remove = 'Inteiro teor'
        if self.descricao.endswith(string_to_remove):
            return self.descricao[:-len(string_to_remove)]
        return self.descricao

    def __str__(self):
        return u'{} - {} - {}'.format(self.projeto, self.data, self.local)
