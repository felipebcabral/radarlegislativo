# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import datetime

from django.test import TestCase

from model_mommy import mommy

from main.models import Projeto, Tramitacao

__all__ = ["TesteTramitacao"]


class TesteTramitacao(TestCase):
    def test_salvar_tramitacao_muda_data_de_atualizacao_do_projeto(self):
        data_original = datetime.date(2015, 01, 01)
        proj = mommy.make(
            Projeto,
            ultima_atualizacao=data_original,
        )

        self.assertEqual(proj.ultima_atualizacao, data_original)

        data_da_tramitacao = datetime.date(2016, 05, 03)
        mommy.make(Tramitacao, projeto=proj, data=data_da_tramitacao)

        self.assertEqual(proj.ultima_atualizacao, data_da_tramitacao)

    def test_mostra_descricao_inteira_se_nao_termina_com_inteiro_teor(self):
        tramitacao = mommy.make(Tramitacao,
                                descricao="descricao")
        self.assertEqual(tramitacao.descricao_limpa, "descricao")

    def test_mostra_descricao_limpa_se_termina_com_inteiro_teor(self):
        tramitacao = mommy.make(Tramitacao,
                                descricao="descricao Inteiro teor")
        self.assertEqual(tramitacao.descricao_limpa, "descricao ")
