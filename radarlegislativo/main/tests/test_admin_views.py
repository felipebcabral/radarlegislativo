# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from django.test import TestCase
from django.urls import reverse

from model_mommy import mommy
import mock

from main.fetcher import FetcherError
from main.forms import ProjetoAddForm
from main.models import Projeto, Tag


class TesteViewCadastroDeProjeto(TestCase):
    fixtures = ["users", "tags"]

    def test_adicionar_projeto_usa_form_de_adicao(self):
        self.client.login(username="admin", password="admin")
        response = self.client.get(reverse("admin:main_projeto_add"))
        used_form = response.context_data['adminform'].form
        self.assertIsInstance(used_form, ProjetoAddForm)

    def test_editar_projeto_usa_form_padrao(self):
        self.client.login(username="admin", password="admin")
        proj = mommy.make('Projeto')
        response = self.client.get(reverse("admin:main_projeto_change",
            args=[proj.id]))
        used_form = response.context_data['adminform'].form
        self.assertNotIsInstance(used_form, ProjetoAddForm)

    def test_adicionar_projeto_usa_o_minimo_de_campos(self):
        self.client.login(username="admin", password="admin")
        response = self.client.get(reverse("admin:main_projeto_add"))
        admin = response.context_data['adminform'].model_admin
        self.assertEqual(admin.fields, ['origem', 'id_site', 'tags'])

    def test_editar_projeto_usa_todos_os_campos(self):
        self.client.login(username="admin", password="admin")
        proj = mommy.make('Projeto')
        response = self.client.get(reverse("admin:main_projeto_change",
            args=[proj.id]))
        admin = response.context_data['adminform'].model_admin
        self.assertEqual(admin.fields, admin.initial_fields)

    @mock.patch('main.forms.scrape_tramitacoes_for_camara_project')
    @mock.patch('main.forms.fetch_camara_project')
    def test_adicionar_projeto_da_camara_chama_fetcher_da_camara(self,
                mocked_fetcher, mocked_tramitacoes_scraper):
        tag_ids = [1, 2]
        proj = mommy.prepare('Projeto', origem=Projeto.CAMARA, id_site=1234)
        mocked_fetcher.return_value = proj
        self.client.login(username="admin", password="admin")
        response = self.client.post(reverse("admin:main_projeto_add"),
                {'origem': Projeto.CAMARA, 'id_site': 1234, 'tags': tag_ids})
        tags = Tag.objects.filter(id__in=tag_ids)
        mocked_fetcher.assert_called()
        args, kwargs = mocked_fetcher.call_args
        self.assertIn(1234, args)
        self.assertEqual(list(kwargs['tags']), list(tags))

    @mock.patch('main.forms.scrape_tramitacoes_for_camara_project')
    @mock.patch('main.forms.fetch_camara_project')
    def test_adicionar_projeto_da_camara_funciona(self, mocked_fetcher,
            mocked_tramitacoes_scraper):
        with self.assertRaises(Projeto.DoesNotExist):
            Projeto.objects.get(origem=Projeto.CAMARA, id_site=1234)

        tag_ids = [1, 2]
        proj = mommy.prepare('Projeto', origem=Projeto.CAMARA, id_site=1234)
        mocked_fetcher.return_value = proj

        self.client.login(username="admin", password="admin")
        response = self.client.post(reverse("admin:main_projeto_add"),
                {'origem': Projeto.CAMARA, 'id_site': 1234, 'tags': tag_ids})
        tags = Tag.objects.filter(id__in=tag_ids)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url,
                reverse('admin:main_projeto_changelist'))

        Projeto.objects.get(origem=Projeto.CAMARA, id_site=1234, tags=tags)


    @mock.patch('main.forms.fetch_tramitacoes_for_senado_project')
    @mock.patch('main.forms.fetch_senado_project')
    def test_adicionar_projeto_do_senado_chama_fetcher_do_senado(self,
                mocked_fetcher, mocked_tramitacoes_scraper):
        tag_ids = [1, 2]
        proj = mommy.prepare('Projeto', origem=Projeto.SENADO, id_site=1234)
        mocked_fetcher.return_value = proj
        self.client.login(username="admin", password="admin")
        response = self.client.post(reverse("admin:main_projeto_add"),
                {'origem': Projeto.SENADO, 'id_site': 1234, 'tags': tag_ids})
        tags = Tag.objects.filter(id__in=tag_ids)
        mocked_fetcher.assert_called()
        args, kwargs = mocked_fetcher.call_args
        self.assertIn(1234, args)
        self.assertEqual(list(kwargs['tags']), list(tags))

    @mock.patch('main.forms.fetch_tramitacoes_for_senado_project')
    @mock.patch('main.forms.fetch_senado_project')
    def test_adicionar_projeto_do_senado_funciona(self, mocked_fetcher,
            mocked_tramitacoes_scraper):
        with self.assertRaises(Projeto.DoesNotExist):
            Projeto.objects.get(origem=Projeto.SENADO, id_site=1234)

        tag_ids = [1, 2]
        proj = mommy.prepare('Projeto', origem=Projeto.SENADO, id_site=1234)
        mocked_fetcher.return_value = proj

        self.client.login(username="admin", password="admin")
        response = self.client.post(reverse("admin:main_projeto_add"),
                {'origem': Projeto.SENADO, 'id_site': 1234, 'tags': tag_ids})
        tags = Tag.objects.filter(id__in=tag_ids)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url,
                reverse('admin:main_projeto_changelist'))

        Projeto.objects.get(origem=Projeto.SENADO, id_site=1234, tags=tags)

    @mock.patch('main.forms.fetch_tramitacoes_for_senado_project')
    @mock.patch('main.forms.fetch_senado_project')
    def test_adicionar_projeto_com_mesma_origem_e_id_nao_duplica(self,
            mocked_fetcher, mocked_tramitacoes_scraper):

        tag_ids = [1, 2]
        tags = Tag.objects.filter(id__in=tag_ids)
        proj = mommy.make('Projeto', origem=Projeto.SENADO, id_site=1234)
        mocked_fetcher.return_value = proj

        self.assertEqual(Projeto.objects.filter(
            origem=Projeto.SENADO, id_site=1234).count(), 1)

        self.client.login(username="admin", password="admin")

        response = self.client.post(reverse("admin:main_projeto_add"),
                {'origem': Projeto.SENADO, 'id_site': 1234, 'tags': tag_ids})

        self.assertEqual(response.status_code, 200)
        self.assertTrue(response.context_data['errors'])

        self.assertEqual(Projeto.objects.filter(
            origem=Projeto.SENADO, id_site=1234).count(), 1)

    @mock.patch('main.forms.fetch_tramitacoes_for_senado_project')
    @mock.patch('main.forms.fetch_senado_project')
    def test_adicionar_projeto_que_nao_existe_mostra_um_erro(self,
            mocked_fetcher, mocked_tramitacoes_scraper):

        tag_ids = [1, 2]
        mocked_fetcher.side_effect = FetcherError
        self.client.login(username="admin", password="admin")

        response = self.client.post(reverse("admin:main_projeto_add"),
                {'origem': Projeto.SENADO, 'id_site': 1234, 'tags': tag_ids})

        self.assertEqual(response.status_code, 200)
        self.assertIn("__all__", response.context_data['adminform'].errors.keys())
