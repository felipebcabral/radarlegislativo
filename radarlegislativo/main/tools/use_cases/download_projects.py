import logging

from main.exceptions import NoneProjectSourceError, UnexpectedProjectSourceError
from main.models import Projeto, Tag
from main.tasks import queue_download
from utils.readers import YamlReader


logger = logging.getLogger(__name__)


class DownloadProjectsFromFile:

    def __init__(self):
        self.downloader = DownloadProjects()
        self.reader = YamlReader

    def execute(self, filename):
        for index, row in enumerate(self.reader.read(filename)):
            tag_ids = list(Tag.objects.filter(
                slug__in=row['tags']
            ).values_list('id', flat=True))
            if not row.get('id_senado') and not row.get('id_camara'):
                raise NoneProjectSourceError(u'row {}: id_senado or id_camara cannot be None.'.format(index))
            external_id = row.get('id_senado') or row.get('id_camara')
            source = Projeto.SENADO if row.get('id_senado') else Projeto.CAMARA
            self.downloader.execute(source, external_id, tag_ids)


class DownloadProjects:

    def execute(self, source, external_id, tags=None):
        tags = tags or []
        if source not in (Projeto.CAMARA, Projeto.SENADO):
            raise UnexpectedProjectSourceError(
                u'{} isn\'t a expected value to Projeto.origem.'.format(source)
            )

        logger.info(
            u'Enfileirando download dados do projeto {}'.format(external_id)
        )
        queue_download.delay(source, external_id, tags)
