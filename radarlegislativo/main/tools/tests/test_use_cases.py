import logging
from model_mommy import mommy
from mock import call, patch
from unittest import TestCase

from main.exceptions import NoneProjectSourceError, UnexpectedProjectSourceError
from main.tools.use_cases import DownloadProjects, DownloadProjectsFromFile


logging.disable(logging.INFO)


class DownloadProjectsTestCase(TestCase):

    def setUp(self):
        self.uc = DownloadProjects()
        self.m_task = patch(
            'main.tools.use_cases.download_projects.queue_download', spec=True
        ).start()

    def test_execute_calls_task_correctly(self):
        self.uc.execute('SE', 1, ['tag1', 'tag2'])
        self.m_task.delay.assert_called_once_with('SE', 1, ['tag1', 'tag2'])

    def test_execute_without_tags_param_calls_task_correctly(self):
        self.uc.execute('SE', 1)
        self.m_task.delay.assert_called_once_with('SE', 1, [])

    def test_execute_raise_exception_when_with_unexpected_source(self):
        self.assertRaises(UnexpectedProjectSourceError, self.uc.execute, 2, 1)


class DownloadProjectsFromFileTestCase(TestCase):

    def setUp(self):
        self.m_reader, self.m_downloader = [
            patch(
                'main.tools.use_cases.download_projects.YamlReader', spec=True
            ).start(),
            patch(
                'main.tools.use_cases.download_projects.DownloadProjects', spec=True
            ).start(),
        ]

        self.uc = DownloadProjectsFromFile()

    def test_initialize_instance_correctly(self):
        assert self.m_downloader() == self.uc.downloader
        assert self.m_reader == self.uc.reader

    def test_execute_calls_reader_and_downloader_correctly(self):
        mommy.make('main.Tag', id=1, slug='priv')
        mommy.make('main.Tag', id=2, slug='acesso')
        data = [{
            'tags': ['priv', 'acesso'],
            'id_senado': 1,
        }, {
            'tags': [],
            'id_camara': 2,
        }]
        self.uc.reader.read.return_value = data

        self.uc.execute('filename')

        self.m_reader.read.assert_called_once_with('filename')
        calls = [
            call('SE', 1, [2, 1]),
            call('CA', 2, []),
        ]
        assert 2 == self.uc.downloader.execute.call_count
        self.uc.downloader.execute.assert_has_calls(calls)

    def test_execute_raises_exception_without_expected_source_id(self):
        data = [{'tags': []}]
        self.uc.reader.read.return_value = data

        self.assertRaises(NoneProjectSourceError, self.uc.execute, 'filename')
