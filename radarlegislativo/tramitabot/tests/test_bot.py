# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase, mock
from model_mommy import mommy
import telepot

from tramitabot.bot import Tramitabot
from tramitabot.models import TramitabotUser


def get_example_message():
    return {
        u'from': {
            u'username': u'rosaluxemburg',
            u'first_name': u'Rosa',
            u'last_name': u'Luxemburg',
            u'is_bot': False,
            u'language_code': u'en-US',
            u'id': 111111111
        },
        u'text': u'/ajuda',
        u'entities': [{
            u'length': 6,
            u'type': u'bot_command',
            u'offset': 0
        }],
        u'chat': {
            u'username': u'rosaluxemburg',
            u'first_name': u'Rosa',
            u'last_name': u'Luxemburg',
            u'type': u'private',
            u'id': 111111111
        },
        u'date': 1522268687,
        u'message_id': 262
    }


class TestTramitabotUserRegistration(TestCase):

    def test_registrar_usuario(self):
        bot = Tramitabot()
        msg = get_example_message()

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())

    def test_deve_poder_registrar_usuario_sem_username(self):
        bot = Tramitabot()
        msg = get_example_message()
        del msg["from"]["username"]

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())

    def test_deve_poder_registrar_usuario_sem_nome(self):
        bot = Tramitabot()
        msg = get_example_message()
        del msg["from"]["first_name"]

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())

    def test_deve_poder_registrar_usuario_sem_sobrenome(self):
        bot = Tramitabot()
        msg = get_example_message()
        del msg["from"]["last_name"]

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())

    def test_deve_poder_registrar_usuario_sem_language_code(self):
        bot = Tramitabot()
        msg = get_example_message()
        del msg["from"]["language_code"]

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.registered_message
        )
        self.assertTrue(TramitabotUser.objects.filter(
                telegram_id=msg["from"]["id"]).exists())

    def test_nao_deve_poder_registrar_usuario_duas_vezes(self):
        bot = Tramitabot()
        msg = get_example_message()

        bot.register_user(msg)

        self.assertEqual(len(TramitabotUser.objects.filter(
            telegram_id=msg["from"]["id"])), 1)

        # change any information so django doesn't think it's exactly
        # the same object
        del msg["from"]["language_code"]

        returned_message = bot.register_user(msg)

        self.assertEqual(
            returned_message,
            Tramitabot.user_already_registered_message
        )
        self.assertEqual(len(TramitabotUser.objects.filter(
            telegram_id=msg["from"]["id"])), 1)


@mock.patch('tramitabot.bot.time.sleep')
class TestTramitabotSendMessages(TestCase):

    @mock.patch('tramitabot.bot.Tramitabot.sendMessage')
    def test_calls_parent_sendMessage_correctly(self,
                            mocked_sendMessage, mocked_time):
        bot = Tramitabot()
        user_id = 111111111
        msg = "Isso é uma mensagem."
        bot.send_message(user_id, msg)
        mocked_sendMessage.assert_called_with(
            111111111,
            msg,
            parse_mode="Markdown",
            disable_web_page_preview=True,
        )

    @mock.patch('tramitabot.bot.Tramitabot.sendMessage')
    def test_calls_parent_sendMessage_twice_for_big_message(self,
                                        mocked_sendMessage, mocked_time):
        bot = Tramitabot()
        bot.chars_limit = 5
        user_id = 111111111
        msg = "1234567890"
        bot.send_message(user_id, msg)
        calls = [
            mock.call(
                111111111,
                "12345",
                parse_mode="Markdown",
                disable_web_page_preview=True,
            ),
            mock.call(
                111111111,
                "67890",
                parse_mode="Markdown",
                disable_web_page_preview=True,
            ),
        ]

        mocked_sendMessage.assert_has_calls(calls)
        mocked_time.assert_called()

    @mock.patch('tramitabot.bot.sys.stderr')
    @mock.patch('tramitabot.bot.Tramitabot.sendMessage')
    @mock.patch('tramitabot.models.TramitabotUserManager.mark_as_blocked')
    def test_marks_user_if_they_blocked_tramitabot(self,
                                mocked_mark_as_blocked, mocked_sendMessage,
                                mocked_stderr, mocked_time):
        bot = Tramitabot()
        user_id = 111111111
        msg = "Isso é uma mensagem."
        error = telepot.exception.BotWasBlockedError('', '', '')
        mocked_sendMessage.side_effect = error
        bot.send_message(user_id, msg)
        mocked_mark_as_blocked.assert_called_with(user_id)

    @mock.patch('tramitabot.bot.Tramitabot.sendMessage')
    @mock.patch('tramitabot.bot.sys.stderr')
    def test_can_handle_TelegramError(self, mocked_stderr, mocked_sendMessage,
                                      mocked_time):
        bot = Tramitabot()
        user_id = 111111111
        msg = "Isso é uma mensagem."
        error = telepot.exception.TelegramError('', '', '')
        mocked_sendMessage.side_effect = error
        bot.send_message(user_id, msg)
        mocked_stderr.write.assert_called()

    @mock.patch('tramitabot.bot.Tramitabot.sendMessage')
    def test_passes_parse_mode_down(self, mocked_sendMessage, mocked_time):
        bot = Tramitabot()
        user_id = 111111111
        msg = "Isso é uma mensagem."
        bot.send_message(user_id, msg, parse_mode="HTML")
        mocked_sendMessage.assert_called_with(
            111111111,
            msg,
            parse_mode="HTML",
            disable_web_page_preview=True,
        )

    @mock.patch('tramitabot.bot.Tramitabot.sendMessage')
    def test_passes_preview_control_down(self, mocked_sendMessage,
                                         mocked_time):
        bot = Tramitabot()
        user_id = 111111111
        msg = "Isso é uma mensagem."
        bot.send_message(user_id, msg, disable_web_page_preview=False)
        mocked_sendMessage.assert_called_with(
            111111111,
            msg,
            parse_mode="Markdown",
            disable_web_page_preview=False,
        )

    @mock.patch('tramitabot.bot.Tramitabot.sendMessage')
    @mock.patch('tramitabot.bot.Tramitabot.prepare_message_by_blocks')
    def test_uses_prepare_message_by_blocks_if_msg_is_list(self,
                                                           mocked_prepare,
                                                           mocked_sendMessage,
                                                           mocked_time):
        bot = Tramitabot()
        user_id = 111111111
        msg = ["isso é uma mensagem", "em blocos"]
        bot.send_message(user_id, msg)
        mocked_prepare.assert_called_with(msg)

    @mock.patch('tramitabot.bot.Tramitabot.sendMessage')
    def test_calls_parent_sendMessage_twice_for_message_with_two_blocks(self,
                                        mocked_sendMessage, mocked_time):
        bot = Tramitabot()
        bot.chars_limit = 5
        user_id = 111111111
        msg = ["12345", "67890"]
        bot.send_message(user_id, msg)
        calls = [
            mock.call(
                111111111,
                "12345",
                parse_mode="Markdown",
                disable_web_page_preview=True,
            ),
            mock.call(
                111111111,
                "67890",
                parse_mode="Markdown",
                disable_web_page_preview=True,
            ),
        ]

        mocked_sendMessage.assert_has_calls(calls)
        mocked_time.assert_called()


class TestGetUltimasTramitacoes(TestCase):

    def test_uses_header_template(self):
        bot = Tramitabot()
        with self.assertTemplateUsed("tramitabot/header_tramitacoes.txt"):
            bot.get_ultimas_tramitacoes(10)

    def test_uses_footer_template(self):
        bot = Tramitabot()
        with self.assertTemplateUsed("tramitabot/footer_tramitacoes.txt"):
            bot.get_ultimas_tramitacoes(10)

    def test_uses_tramitacao_template(self):
        mommy.make('Tramitacao')
        bot = Tramitabot()
        with self.assertTemplateUsed("tramitabot/tramitacao.txt"):
            bot.get_ultimas_tramitacoes(10)

    def test_returns_blocks_to_be_prepared_as_messages(self):
        bot = Tramitabot()
        blocks = bot.get_ultimas_tramitacoes(10)
        self.assertIsInstance(blocks, list)

    def test_returns_right_amount_of_blocks(self):
        mommy.make('Tramitacao', _quantity=20)
        bot = Tramitabot()
        blocks = bot.get_ultimas_tramitacoes(10)
        self.assertEqual(len(blocks), 12)  # n + header and footer


class TestPrepareMessage(TestCase):

    def test_string_menor_que_o_limite(self):
        bot = Tramitabot()
        msg = "Mensagem pequena."
        messages = bot.prepare_message(msg)
        self.assertEqual(messages, [msg])

    def test_quebra_string_maior_que_o_limite(self):
        bot = Tramitabot()
        bot.chars_limit = 5
        msg = "1234567890"
        messages = bot.prepare_message(msg)
        self.assertEqual(messages, ["12345", "67890"])

    def test_quebra_string_tres_vezes_maior_que_o_limite(self):
        bot = Tramitabot()
        bot.chars_limit = 5
        msg = "123456789012345"
        messages = bot.prepare_message(msg)
        self.assertEqual(messages, ["12345", "67890", "12345"])


class TestPrepareMessageByBlocks(TestCase):

    def test_um_bloco_que_cabe_vem_em_uma_mensagem(self):
        bot = Tramitabot()
        blocks = ["*Mensagem pequena*"]
        messages = list(bot.prepare_message_by_blocks(blocks))
        self.assertEqual(messages, ["*Mensagem pequena*"])

    def test_dois_blocos_que_cabem_vem_em_uma_mensagem(self):
        bot = Tramitabot()
        blocks = ["*Mensagem pequena*; ", "outro _trecho_"]
        messages = list(bot.prepare_message_by_blocks(blocks))
        self.assertEqual(messages, ["*Mensagem pequena*; outro _trecho_"])

    def test_dois_blocos_que_nao_cabem_vem_em_duas_mensagens(self):
        bot = Tramitabot()
        bot.chars_limit = 25
        blocks = ["*Mensagem que cabe*; ", "outro _trecho_"]
        messages = list(bot.prepare_message_by_blocks(blocks))
        self.assertEqual(messages, ["*Mensagem que cabe*; ", "outro _trecho_"])

    def test_bloco_maior_que_limite_quebra(self):
        bot = Tramitabot()
        bot.chars_limit = 10
        blocks = ["*Bloco que não cabe*"]
        with self.assertRaises(ValueError):
            list(bot.prepare_message_by_blocks(blocks))
