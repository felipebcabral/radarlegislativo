#  -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals

import datetime
import operator
from functools import reduce

from django.shortcuts import redirect
from django.contrib.syndication.views import Feed
from django.db.models import Q
from django.urls import reverse_lazy
from django.views.generic import DetailView, ListView, TemplateView, View
from django.http import JsonResponse, Http404

from haystack.generic_views import FacetedSearchView

from agenda.models import Comissao, Evento
from main.models import Projeto, Tramitacao
from website.models import WebsiteOptions

from graph.core import generate_graph
from search.forms import DefaultFullSearchForm
from website.forms import FilterForm, FormListagemSemanal


class Principal(FacetedSearchView):
    model = Projeto
    template_name = "website/index.html"
    context_object_name = "projetos"
    paginate_by = 10
    form_class = DefaultFullSearchForm
    facet_fields = ["origem"]

    def get_ordering(self):
        ordem = self.request.GET.get("ordem")
        # Isso é um conjunto de if por que as ordenações não são
        # diretamente relacionadas aos campos do modelo
        if ordem == u'novos':
            return '-apresentacao'
        if ordem == u'urgentes':
            return '-urgente'
        if ordem == u'arquivados':
            return '-arquivado'
        else:
            return '-ultima_atualizacao'

    def get_queryset(self, *args, **kwargs):
        qs = super(Principal, self).get_queryset(*args, **kwargs)
        filter_form = FilterForm(self.request.GET)
        if filter_form.is_valid():
            origem = filter_form.cleaned_data['origem']
            if origem:
                qs = qs.filter(origem__in=origem)
            categorias = filter_form.cleaned_data['categoria']
            if categorias:
                query = reduce(
                    operator.and_,
                    (Q(categoria=cat) for cat in categorias)
                )
                qs = qs.filter(query)
            impacto = filter_form.cleaned_data['impacto']
            if impacto:
                qs = qs.filter(impacto__in=impacto)
            comissao = filter_form.cleaned_data['comissao']
            if comissao:
                nome_da_comissao = Comissao.objects.get(pk=comissao).nome
                qs = qs.filter(comissao=nome_da_comissao)
        return qs

    def get_context_data(self, *args, **kwargs):
        context = super(Principal, self).get_context_data(*args, **kwargs)
        context["filter_form"] = FilterForm(self.request.GET)
        return context


class TramitacoesFeed(Feed):
    title = ("Projetos de Lei — Projetos de Lei que são relevantes "
             "para a privacidade, liberdade de expressão, acesso, "
             "inovação e questões de gênero no meio digital")

    description = "Feed unificado dos PL's na página radarlegislativo.org"

    def link(self):
        return "https://radarlegislativo.org/{}".format(
            reverse_lazy("feed-todos")
        )

    def items(self):
        return Tramitacao.objects.order_by("-data")[:25]

    def item_title(self, item):
        return "{} - {}".format(item.projeto.nome, item.local)

    def item_description(self, item):
        return item.descricao

    def item_pubdate(self, item):
        return datetime.datetime.combine(item.data, datetime.time())

    def item_guid(self, item):
        return "{}-{}".format(item.get_absolute_url(), item.id)

    item_guid_is_permalink = True


class DetalheProjeto(DetailView):
    model = Projeto
    template_name = "website/projeto.html"


class ViewListagemSemanal(ListView):
    _date = None
    _origem = None

    @property
    def date(self):
        if self._date is None:
            self.validate_form()
        return self._date

    @property
    def semanas_proximas(self):
        inicios = [
            self.dias_da_semana[0] + datetime.timedelta(days=n * 7)
            for n in range(-3, 4)
        ]
        semanas = []
        for dia in inicios:
            semanas.append((dia, (dia + datetime.timedelta(days=6))))
        return semanas

    @property
    def dias_da_semana(self):
        inicio_da_semana = self.date - datetime.timedelta(self.date.weekday())
        return [
            inicio_da_semana + datetime.timedelta(days=n) for n in range(7)
        ]

    def validate_form(self):
        date = datetime.date.today()
        if self.request.GET.get('data'):
            self.form = FormListagemSemanal(self.request.GET)
            if self.form.is_valid():
                date = self.form.cleaned_data['data']
                self._origem = self.form.cleaned_data['origem']
        else:
            self.form = FormListagemSemanal()
        self._date = date

    def deve_filtrar_pela_origem(self):
        if self._origem is None:
            self.validate_form()
        return self._origem and self._origem != FormListagemSemanal.origem_default

    def get_queryset(self):
        raise NotImplementedError()

    def get_context_data(self, **kwargs):
        context = super(ViewListagemSemanal, self).get_context_data(**kwargs)

        hoje = datetime.date.today()

        context['data'] = self.date
        context['hoje'] = hoje
        context['semanas_proximas'] = self.semanas_proximas
        context['dias_da_semana'] = self.dias_da_semana
        context['inicio_da_semana_atual'] = (
            hoje - datetime.timedelta(hoje.weekday()))
        context['form'] = self.form

        return context


class Agenda(ViewListagemSemanal):
    model = Evento
    template_name = "website/agenda.html"

    def get_queryset(self):
        qs = Evento.objects.na_mesma_semana_que_o_dia(self.date)
        if self.deve_filtrar_pela_origem():
            qs = qs.filter(comissao__origem=self._origem)
        return qs


class Tramitabot(TemplateView):
    template_name = "website/tramitabot.html"


class CenarioLegislativo(TemplateView):
    template_name = "website/cenario_legislativo.html"


class NovosPLs(ViewListagemSemanal):
    context_object_name = "novos_pls"
    model = Projeto
    template_name = "website/novos_pls.html"

    def get_queryset(self):
        qs = Projeto.objects.na_mesma_semana_que_o_dia(self.date).\
             order_by('cadastro')
        if self.deve_filtrar_pela_origem():
            qs = qs.filter(origem=self._origem)
        return qs


class Sobre(TemplateView):
    template_name = "website/sobre.html"


class GrafoJson(View):
    def get(self, request):
        return JsonResponse(generate_graph())


def redirect_to_graphcommons(request):
    options = WebsiteOptions.objects.first()
    if options is None:
        raise Http404
    return redirect(options.graph_url)


class CustomCSS(TemplateView):
    content_type = 'text/css'
    template_name = 'website/custom.css'
