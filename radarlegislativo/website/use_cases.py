from django.core.cache import cache

from main.models import Tag


class UpdateCachedTags:

    def execute(self):
        tags = list(Tag.objects.all().values(
            'id', 'slug', 'nome', 'icon'
        ))
        cache.set('tags', tags, 600)
