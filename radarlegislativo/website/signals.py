from django.db.models.signals import post_save
from django.dispatch import receiver

from main.models import Tag
from website.use_cases import UpdateCachedTags


@receiver(post_save, sender=Tag)
def update_cached_tags(sender, **kwargs):
    UpdateCachedTags().execute()
