# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from __future__ import unicode_literals

from urllib2 import urlparse

from django.test import TestCase, RequestFactory, mock, override_settings
from django.urls import reverse

from main.models import Projeto
from website.models import WebsiteOptions
from website.templatetags.website_tags import change_url_parameter
from website.views import Principal


__all__ = ["TestPaginaPrincipal", "TestOrdenacaoDaPaginaPrincipal",
           "TestFiltrosDaPaginaPrincipal", "TestChangeUrlParameterTag",
           "TestRedirectToGraphcommons"]


class TestPaginaPrincipal(TestCase):
    fixtures = ["projetos", "tramitacoes", "tags"]

    def test_pagina_principal_retorna_200(self):
        response = self.client.get(reverse("website:principal"))
        self.assertEqual(response.status_code, 200)

    def test_pagina_principal_usa_o_template_certo(self):
        response = self.client.get(reverse("website:principal"))
        self.assertTemplateUsed(response, "website/index.html")

    def test_pagina_principal_inclui_projetos_no_contexto(self):
        response = self.client.get(reverse("website:principal"))
        self.assertIn("projetos", response.context)

    @override_settings(TRAMITABOT_API_TOKEN=None)
    def test_pagina_principal_inclui_textos_customizados(self):
        WebsiteOptions.objects.create(long_title="Foobar", graph_url=None)
        response = self.client.get(reverse("website:principal"))
        self.assertContains(response, u"<h1>Foobar</h1>")
        self.assertNotContains(response, u"Tramitabot")
        self.assertNotContains(response, u"Grafo de PLS")


class TestOrdenacaoDaPaginaPrincipal(TestCase):
    """
    Estamos testando só o método get_ordering por que a
    FacetedSearchView do haystack lida com a ordenação a partir do
    resultado dele.
    """

    def setUp(self):
        self.factory = RequestFactory()

    def _get_view(self, *args, **kwargs):
        view = Principal()
        view.request = self.factory.get(reverse("website:principal"),
                                        data=kwargs)
        return view

    def test_pagina_principal_ordena_por_ultima_tramitacao_por_padrao(self):
        view = self._get_view()
        self.assertEqual(view.get_ordering(), '-ultima_atualizacao')

    def test_pagina_principal_ordena_por_novos(self):
        view = self._get_view(ordem="novos")
        self.assertEqual(view.get_ordering(), '-apresentacao')

    def test_pagina_principal_ordena_por_urgentes(self):
        view = self._get_view(ordem="urgentes")
        self.assertEqual(view.get_ordering(), '-urgente')

    def test_pagina_principal_ordena_por_arquivados(self):
        view = self._get_view(ordem="arquivados")
        self.assertEqual(view.get_ordering(), '-arquivado')


class TestFiltrosDaPaginaPrincipal(TestCase):
    """
    Estamos mockando o super() de get_queryset para facilitar os
    testes, já que o backend simple do haystack não suporta faceting,
    o que nos impede de testar o funcionamento completo da view.

    """
    fixtures = ["projetos", "tramitacoes", "tags"]

    def setUp(self):
        self.factory = RequestFactory()

    def _get_view(self, *args, **kwargs):
        view = Principal()
        view.request = self.factory.get(reverse("website:principal"),
                                        data=kwargs)
        return view

    @mock.patch('website.views.FacetedSearchView.get_queryset')
    def test_pagina_principal_nao_filtra_por_padrao(self, mocked_parent):
        view = self._get_view()
        mocked_parent.return_value = Projeto.objects.all()
        qs = view.get_queryset()
        self.assertQuerysetEqual(
            qs,
            Projeto.objects.all(),
            transform=lambda x: x
        )

    @mock.patch('website.views.FacetedSearchView.get_queryset')
    def test_pagina_principal_filtra_pela_camara(
            self, mocked_parent):
        view = self._get_view(origem="CA")
        mocked_parent.return_value = Projeto.objects.all()
        qs = view.get_queryset()
        self.assertQuerysetEqual(
            qs,
            Projeto.objects.filter(origem__in=["CA"]),
            transform=lambda x: x
        )

    @mock.patch('website.views.FacetedSearchView.get_queryset')
    def test_pagina_principal_filtra_pelo_senado(
            self, mocked_parent):
        view = self._get_view(origem="SE")
        mocked_parent.return_value = Projeto.objects.all()
        qs = view.get_queryset()
        self.assertQuerysetEqual(
            qs,
            Projeto.objects.filter(origem__in=["SE"]),
            transform=lambda x: x
        )

    @mock.patch('website.views.FacetedSearchView.get_queryset')
    def test_pagina_principal_aceita_mais_de_uma_origem(self,
                                                        mocked_parent):
        view = self._get_view(origem="SE,CA")
        mocked_parent.return_value = Projeto.objects.all()
        qs = view.get_queryset()
        self.assertQuerysetEqual(
            qs,
            Projeto.objects.filter(origem__in=["SE", "CA"]),
            transform=lambda x: x,
            ordered=False,
        )


class TestChangeUrlParameterTag(TestCase):

    def setUp(self):
        self.factory = RequestFactory()

    def test_empty_querystring_returns_only_new_value(self):
        request = self.factory.get('/', data={})
        new_url = change_url_parameter(request, 'key', 'value')
        result = urlparse.parse_qs(new_url[1:])
        self.assertEqual(result['key'], ['value'])

    def test_adding_new_key_does_not_change_previous(self):
        request = self.factory.get('/', data={'key': 'value'})
        new_url = change_url_parameter(request, 'new_key', 'new_value')
        result = urlparse.parse_qs(new_url[1:])
        self.assertEqual(result['key'], ['value'])
        self.assertEqual(result['new_key'], ['new_value'])

    def test_changing_a_key_changes_its_value(self):
        request = self.factory.get('/', data={'key': 'value'})
        new_url = change_url_parameter(request, 'key', 'new_value')
        result = urlparse.parse_qs(new_url[1:])
        self.assertEqual(result['key'], ['new_value'])

    def test_changing_a_key_does_not_change_other_keys_value(self):
        request = self.factory.get('/', data={'key_1': 'value_1',
                                              'key_2': 'value_2'})
        new_url = change_url_parameter(request, 'key_1', 'new_value')
        result = urlparse.parse_qs(new_url[1:])
        self.assertEqual(result['key_1'], ['new_value'])
        self.assertEqual(result['key_2'], ['value_2'])


class TestRedirectToGraphcommons(TestCase):

    def test_should_redirect_to_correct_url_if_website_options_exists(self):
        options = WebsiteOptions(
            graph_url="https://www.example.com/",
            tracking_code=""
        )
        options.save()
        response = self.client.get(reverse("website:grafo"))
        self.assertRedirects(response, options.graph_url,
                             fetch_redirect_response=False)

    def test_should_raise_404_if_there_is_no_websiteoptions(self):
        response = self.client.get(reverse("website:grafo"))
        self.assertEqual(response.status_code, 404)


class TestCustomCSS(TestCase):

    def test_custom_css(self):
        WebsiteOptions.objects.create(header_background_color_light="foobar")
        response = self.client.get(reverse("website:custom-css"))
        self.assertContains(response, u"foobar")
