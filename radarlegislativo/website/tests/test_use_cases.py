from django.core.cache import cache
from django.db.models.signals import post_save
from django.test import TestCase
from model_mommy import mommy

from main.models import Tag
from website.signals import update_cached_tags
from website.use_cases import UpdateCachedTags


class UpdateCachedTagsTestCase(TestCase):

    def setUp(self):
        # delete cache from another tests
        cache.delete('tags')
        post_save.disconnect(update_cached_tags, sender=Tag)

    def tearDown(self):
        post_save.connect(update_cached_tags, sender=Tag)

    def test_uc_execute_updates_cache_correctly(self):
        # testing if signal was disconnected
        tag = mommy.make(Tag)
        assert cache.get('tags') is None

        UpdateCachedTags().execute()
        cached = cache.get('tags')
        assert 1 == len(cached)
        assert tag.id == cached[0]['id']
        assert tag.slug == cached[0]['slug']
        assert tag.nome == cached[0]['nome']
