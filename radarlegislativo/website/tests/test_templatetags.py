# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2018 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
from django.template import Context, Template
from django.test import TestCase
from model_mommy import mommy

__all__ = ["TestGetIconFromTagId"]


class TestGetIconFromTagId(TestCase):
    fixtures = ["tags"]

    template_base = ("{{% load website_tags %}}"
                     "{{% get_icon_html_from_tag_id {} %}}")

    def test_should_return_correct_icon_for_tag_that_has_one(self):
        html = Template(self.template_base.format(1)).render(Context({}))

        self.assertIn(u'acesso', html)
        self.assertIn(u'Acesso', html)

    def test_should_return_template_for_tag_that_doesnt_have_icon(self):
        """
        Even if the icon doesn't exist, we should include it and let a
        404 happen in the browser
        """
        tag = mommy.make('Tag')
        html = Template(self.template_base.format(tag.id)).render(Context({}))
        self.assertIn(tag.slug, html)
        self.assertIn(tag.nome, html)

    def test_should_return_nothing_for_tag_that_doesnt_exist(self):
        html = Template(self.template_base.format(999)).render(Context({}))
        self.assertEqual(html.strip(), '')

    def test_if_data_toggle_is_false_use_alt_instead_of_title(self):
        template = ("{% load website_tags %}"
                    "{% get_icon_html_from_tag_id 1 data_toggle=False %}")
        html = Template(template).render(Context({}))

        self.assertNotIn(u'data-toggle="tooltip"', html)
        # Quanto temos data_toggle, temos title no lugar do alt
        self.assertNotIn(u'title="Acesso"', html)
        self.assertIn(u'alt="ícone de Acesso"', html)

    def test_should_pass_data_toggle_down_to_template(self):
        template = ("{% load website_tags %}"
                    "{% get_icon_html_from_tag_id 1 data_toggle=True %}")
        html = Template(template).render(Context({}))

        self.assertIn(u'data-toggle="tooltip"', html)
        # Quanto temos data_toggle, temos title no lugar do alt
        self.assertIn(u'title="Acesso"', html)
        self.assertNotIn(u'alt=', html)

    def test_markdown_text(self):
        template = ("{% load website_tags %}"
                    "{{ title|safe|markdown_text }}")
        html = Template(template).render(Context({'title': '**Foo** bar'}))
        self.assertEqual(u'<p><strong>Foo</strong> bar</p>', html)
