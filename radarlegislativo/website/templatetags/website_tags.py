#-*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2017 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import markdown
from django import template
from django.core.cache import cache
from django.core.urlresolvers import reverse

from website.models import WebsiteOptions

register = template.Library()

@register.simple_tag
def change_url_parameter(request, field, new_value):
    params = request.GET.copy()
    params[field] = new_value
    return "?{}".format(params.urlencode())

@register.simple_tag
def get_title_from_label(label):
    title_map = {
        u'🙂️': u"Bom",
        u'😐': u"Atenção: pode ameaçar direitos",
        u'🙁': u"Ruim",
    }
    return title_map.get(label, label)

@register.simple_tag
def link_filtro(slug):
    tags = cache.get('tags')
    tag_id = [t['id'] for t in tags if t['slug'] == slug][0]
    return '{}?categoria={}'.format(reverse('website:principal'), tag_id)

@register.filter(is_safe=True)
def markdown_text(text):
    return markdown.markdown(text)

@register.inclusion_tag("website/tag_icon.html")
def get_icon_html_from_tag_id(tag_id, data_toggle=True):
    try:
        tags = cache.get('tags')
        tag = [t for t in tags if t['id'] == tag_id][0]

        return {
            'slug': tag['slug'],
            'nome': tag['nome'],
            'icon': tag['icon'],
            'data_toggle': data_toggle,
        }
    except IndexError:
        return {}


@register.simple_tag
def get_cached_tags():
    return cache.get('tags')
