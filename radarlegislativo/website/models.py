# -*- coding: utf-8 -*-
# This file is part of Radar Legislativo
# Copyright © 2016 codingrights
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""

O modelo ``models.WebsiteOptions`` foi feito para ter somente uma instância, que pode ser criada com uma URL do grafo que será linkada na página principal.

"""

from __future__ import unicode_literals

from django.db import models
from django.core.exceptions import ValidationError
from django.utils.encoding import python_2_unicode_compatible

class WebsiteOptions(models.Model):
    graph_url = models.CharField('URL do grafo', max_length=255, null=True, blank=True)
    tracking_code = models.CharField('Código de rastreamento', null=True, blank=True, max_length=1000)

    custom_favicon = models.CharField('URL do favicon', max_length=255, null=True, blank=True, default=None)

    short_title = models.CharField('Título curto', max_length=255, default=u'Radar')
    long_title = models.CharField('Título longo', max_length=255, default=u'Radar legislativo')
    headline = models.CharField('Subtítulo', max_length=255, default=u'acesso · gênero · liberdade de expressão · privacidade · inovação · direito do consumidor')

    small_logo = models.CharField('Logo pequeno', max_length=255, default='/static/img/antena-robo.svg')
    large_logo = models.CharField('Logo grande', max_length=255, default='/static/img/cabecalho-robo.jpg')

    footer_headline = models.CharField('Subtítulo do rodapé', max_length=255, default='<b>Radar.</b> Tudo sobre leis e direitos humanos no meio digital.')

    maintained_by_name = models.CharField('Nome de quem mantém o site', max_length=255,  default=u'Um projeto mantido pela Coding Right')
    maintained_by_logo = models.CharField('Logo de quem mantém o site', max_length=255,  default='/static/img/logoCR.svg')
    maintained_by_url = models.CharField('URL de quem mantém o site', max_length=255,  default='https://www.codingrights.org')

    maintained_by_extra_text = models.CharField('Texto do link extra de quem mantém o site', max_length=255,  default=u'Assine o nosso boletim')
    maintained_by_extra_url = models.CharField('Link extra de quem mantém o site', max_length=255,  default='https://antivigilancia.org/pt/boletim/')

    maintained_by_twiiter = models.CharField('Twitter de quem mantém o site (somente nome de usuário)', max_length=255,  default=u'codingrights')

    text_highlight_color = models.CharField('Cor de textos de destaque', max_length=255,  default='#5f63bc')
    header_background_color_dark = models.CharField('Cor de fundo da barra superior (menu)', max_length=255,  default='#5256a0')
    header_background_color = models.CharField('Cor de fundo da barra intermediária (título do site)', max_length=255,  default='#5f63bc')
    header_background_color_light = models.CharField('Cor de funto da barra inferior (link para agenda)', max_length=255,  default='#a1c7f6')

    modified_date = models.DateTimeField('Última modificação', auto_now=True)

    class Meta:
        verbose_name = "Opções"
        verbose_name_plural = "Opções"

    def clean(self):
        if WebsiteOptions.objects.exists() and not self.pk:
            raise ValidationError('Só pode haver um objeto "Opções". Edite o que já existe.')
        return super(WebsiteOptions, self).clean()
